#!/usr/bin/python
from PIL import Image
import sys

def run():
    im = Image.open(sys.argv[1])
    im = im.convert('RGB')
    pal = []
    uniq = 0
    for y in range(0, im.height):
        for x in range(0, im.width):
            c = im.getpixel((x, y,))
            if c not in pal:
                pal.append(c)

    print('bootImage.palette db ', end='')
    for i in range(0, len(pal)):
        print('0x%02x,0x%02x,0x%02x' % (pal[i][0]>>2, pal[i][1]>>2, pal[i][2]>>2,), end='')
        if i != len(pal) - 1:
            print(',', end='')
    print()
    print('bootImage.palette.length dw 0x%04x' % (len(pal),))
    print()
    print('bootImage.data db ', end='')

    lc = im.getpixel((0,0,))
    cc = (0,0,0,)
    rl = 1
    rll = 0
    for y in range(0, im.height):
        for x in range(0, im.width):
            if y == 0 and x == 0:
                continue
            cc = im.getpixel((x, y,))
            if cc == lc:
                if rl == 255:
                    pi = pal.index(lc)
                    print('0x%02x,' % 255, end='')
                    print('0x%02x,' % rl, end='')
                    print('0x%02x,' % pi, end='')
                    rl = 1
                    rll += 3
                else:
                    rl += 1
            else:
                pi = pal.index(lc)
                if rl > 1:
                    print('0x%02x,' % 255, end='')
                    print('0x%02x,' % rl, end='')
                    print('0x%02x,' % pi, end='')
                    rl = 1
                    rll += 3
                else:
                    print('0x%02x,' % pi, end='')
                    rll += 1
                    if pi == 255:
                        print('0x%02x,' % (1,), end='')
                        rll += 1
            lc = cc
    if rl > 0:
        if rl > 1:
            print('0x%02x,' % 255, end='')
            print('0x%02x,' % rl, end='')
            print('0x%02x' % pi, end='')
            rl = 1
            rll += 3
        else:
            if pi == 255:
                print('0x%02x,' % (pi,), end='')
                print('0x%02x' % (1,), end='')
                rll += 2
            else:
                print('0x%02x' % (pi,), end='')
                rll += 1
    print()
    print('bootImage.data.length dd 0x%08x' % (rll,))

run()
