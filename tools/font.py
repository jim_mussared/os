#!/usr/bin/python
from PIL import Image
import sys

def printChar(c, im, x, w, h):
    if c == 'a':
        print('.alphalower:')
    elif c == 'A':
        print('.alphaupper:')
    elif c == '0':
        print('.numeric:')
    print('  db ', end='')

    for y in range(0, h):
        b = 0
        for xx in range(w-1, -1, -1):
            b = b << 1
            p = im.getpixel((x+xx, y,))
            if min(p) == max(p) == 0:
                b |= 1
        print('0x%02x%c' % (b, ' ' if y == h-1 else ',',), end='')
    if c == '\\':
        print('; backslash')
    else:
        print('; %c' % (c,))

def run():
    im = Image.open(sys.argv[1])
    print('bootImage.font:')
    x = 0
    i = 0
    while x < im.width:
        printChar(chr(ord(' ') + i), im, x, 6, 8)
        i += 1
        x += 6

run()
