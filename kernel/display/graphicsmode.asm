
graphics.drawBootScreen:
	xor eax, eax
	xor ecx, ecx
	
	call graphics.vSyncWait
	
	; allow update of all registers (0x3c6 is the mask port)
	mov dx, 0x3c6
	mov al, 0xff
	out dx, al
	
	; start at palette index 0 (write the index to port 0x3c8 )
	mov dx, 0x3c8
	mov al, 0
	out dx, al
	
	; write the palette (writing each triplet to 0x3c9)
	mov esi, bootImage.palette
	mov cx, [bootImage.palette.length]
	mov ax, 3
	mul cx
	mov cx, ax
	mov dx, 0x3c9
	rep outsb
	
	mov cx, 256
	sub cx, [bootImage.palette.length]
	mov ax, 3
	mul cx
	mov cx, ax
	xor al, al
	rep outsb
	
	mov esi, bootImage.data				; input data
	mov edi, 0xa0000					; video memory
	mov ecx, [bootImage.data.length]
	
	.inputdataLoop:
	
		; every byte is either a color or 0xff
		; 0xff means next byte is a length, followed by a color
		
		lodsb
		cmp al, 0xff
		je graphics.drawBootScreen.doRun
		
		; single color
		stosb
		loop graphics.drawBootScreen.inputdataLoop
		jmp graphics.drawBootScreen.drawFinished
		
		.doRun:
			add ecx, 2
			push ecx
				; get length into cl
				lodsb
				xor ecx, ecx
				mov cl, al
				
				; load color and repeatedly store it
				lodsb
				rep stosb
			pop ecx
	
	loop graphics.drawBootScreen.inputdataLoop
	
	.drawFinished:
	
	; Draw over the preset colors
	mov al, graphics.color.background
	mov ecx, graphics.color.background
	mov edi, 0xa0000
	rep stosb
ret

graphics.drawString  ; (sz, x, y, foreground color, background color)
push ebp
mov ebp, esp
pushad

	mov esi, [ebp+8]
	mov ecx, [ebp+12] ; x
	mov ebx, [ebp+16] ; y
	
	push dword [ebp+24] ; bgcolor - dont need to push this more than once
	push dword [ebp+20] ; fgcolor - dont need to push this more than once
	
		.nextChar:
		xor eax, eax
		lodsb
		cmp al, 0
		je graphics.drawString.finish
		;cmp al, 0x20
		;je graphics.drawString.printSpace
		cmp al, 0x0a
		je graphics.drawString.printNewLine
		
		sub eax, 0x20	; the bitmaps start at the character space
		
		push ebx ; y
		push ecx ; x
		push eax ; char
			call graphics.drawChar
		add esp, 12
		
		;.printSpace:
		add ecx, 6
		cmp ecx, 314
		ja graphics.drawString.printNewLine
		jmp graphics.drawString.nextChar
		
		.printNewLine:
		add ebx, 8
		mov ecx, [ebp+12]
		jmp graphics.drawString.nextChar
	
	.finish:
	
	add esp, 8
		
popad
mov esp, ebp
pop ebp
ret

graphics.drawDWord:  ; (dword, x, y, fg, bg)
push ebp
mov ebp, esp
pushad
	push dword [ebp+24]
	push dword [ebp+20]
	push dword [ebp+16]
	
	mov edx, [ebp+12]
	push edx
	mov eax, [ebp+8]
	mov ebx, eax
	shr eax, 16
	push eax
	call graphics.drawWord
	add esp, 8
	add edx, 24
	push edx
	push ebx
	call graphics.drawWord
	add esp, 20
popad
mov esp, ebp
pop ebp
ret

graphics.drawWord:  ; (word, x, y, fg, bg)
push ebp
mov ebp, esp
pushad
	push dword [ebp+24]
	push dword [ebp+20]
	push dword [ebp+16]
	
	mov edx, [ebp+12]
	push edx
	mov eax, [ebp+8]
	xor ebx, ebx
	xor ecx, ecx
	mov bl, ah
	mov cl, al
	push ebx
	call graphics.drawByte
	add esp, 8
	
	add edx, 12
	push edx
	push ecx
	call graphics.drawByte
	add esp, 20
popad
mov esp, ebp
pop ebp
ret

graphics.drawByte:  ; (byte, x, y, fg, bg)
push ebp
mov ebp, esp
pushad
	push dword [ebp+24]
	push dword [ebp+20]
	push dword [ebp+16]
	
	push dword [ebp+12]
	mov eax, [ebp+8]
	xor ebx, ebx
	mov bl, al
	shr eax, 4
	push eax
	call graphics.drawNibble
	add esp, 8
	
	mov eax, [ebp+12]
	add eax, 6
	push eax
	push ebx
	call graphics.drawNibble
	
	add esp, 20
popad
mov esp, ebp
pop ebp
ret

graphics.drawNibble:  ; (nibble, x, y, foreground, background)
push ebp
mov ebp, esp
pushad

	push dword [ebp+24]
	push dword [ebp+20]
	push dword [ebp+16]
	push dword [ebp+12]
	
	mov eax, [ebp+8]
	and eax, 0x0f
	cmp al, 0x0A
	jb graphics.drawNibble.finish
	add al, 0x27
	.finish:
	add al, 0x10
	
	push eax
	call graphics.drawChar
	add esp, 20

popad
mov esp, ebp
pop ebp
ret


graphics.drawChar:  ; (char, x, y, foreground color, background color)
push ebp
mov ebp, esp
pushad

	mov esi, bootImage.font
	mov eax, 8
	mul dword [ebp+8]
	add esi, eax
	
	mov edi, 0xa0000
	mov eax, 320
	mul dword [ebp+16]
	add edi, eax
	add edi, [ebp+12]
	
	mov ecx, 8
	.rowLoop:
		lodsb
		mov ebx, eax
		push ecx
		mov ecx, 6
		.colLoop:
			mov al, bl
			and al, 1
			jz graphics.drawChar.skipCol
				mov al, [ebp+20]
				stosb
				shr ebx, 1
				loop graphics.drawChar.colLoop
				jmp graphics.drawChar.finishCol
			.skipCol:
				mov al, [ebp+24]
				stosb
				shr ebx, 1
				loop graphics.drawChar.colLoop
		.finishCol:
		pop ecx
		add edi, 314
		loop graphics.drawChar.rowLoop
	
popad
mov esp, ebp
pop ebp
ret


;	void vsync()
;	{
;		/* wait until any previous retrace has ended */
;		do {
;		} while (inportb(0x3DA) & 8);
;
;		/* wait until a new retrace has just begun */
;		do {
;		} while (!(inportb(0x3DA) & 8));
;	}

graphics.vSyncWait:
pushad
	.vSyncFinishWait
	mov dx, 0x3da
	in al, dx
	and al, 8
	jz graphics.vSyncWait.vSyncFinishWait
	
	.vSyncBeginWait
	in al, dx
	and al, 8
	jnz graphics.vSyncWait.vSyncBeginWait
popad
ret

graphics.drawProgressBar:
pushad
	mov edi, 0xa0000
	add edi, 62080 ; 320x194
	add edi, 238
	
	mov edx, 0
	mov dl, [graphics.drawProgressBar.width]
	
	mov ecx, 3 ; height
	.lineLoop:
		push ecx
			mov ecx, 0
			mov cl, dl
			
			mov al, 0xec
			rep stosb
			mov al, 0xdf
			stosb
			
			sub edi, edx
			add edi, 319
		pop ecx
	loop graphics.drawProgressBar.lineLoop
	
	inc byte [graphics.drawProgressBar.width]
popad
ret
.width db 0x01
