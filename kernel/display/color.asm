;	0-3 Foreground color (16 posible values) 
;	4-6 Background color (8 possible values) 
;	7 Blinking (0 = off, 1 = on) 

%define text.color.black			0x00
%define text.color.blue			0x01
%define text.color.green			0x02
%define text.color.cyan			0x03
%define text.color.red			0x04
%define text.color.magenta		0x05
%define text.color.brown			0x06
%define text.color.lightgray		0x07
%define text.color.darkgray		0x08
%define text.color.lightblue		0x09
%define text.color.lightgreen	0x0a
%define text.color.lightcyan		0x0b
%define text.color.lightred		0x0c
%define text.color.lightmagenta	0x0d
%define text.color.yellow		0x0e
%define text.color.white			0x0f

%define graphics.color.black		0x00
%define graphics.color.lighgray		0x01
%define graphics.color.gray			0x02
%define graphics.color.darkgray		0x03
%define graphics.color.charcoal		0x04
%define graphics.color.red			0x05
%define graphics.color.yellow		0x06
%define graphics.color.green		0x07
%define graphics.color.cyan			0x08
%define graphics.color.blue			0x09
%define graphics.color.magenta		0x0a
%define graphics.color.background	0x0b

%define blink.on			10000000b
%define blink.off			00000000b

%define toColor(fore,back,blink) (fore | back<<4 | blink)
%define toColor(fore,back) (fore | back<<4)
%define toColor(fore) (fore | text.color.black<<4)