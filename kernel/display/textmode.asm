draw:
	.xPos db 0x0
	.yPos db 0x0

print.numberString:
pushad
	mov edx, eax
	lodsb
	mov ah, al
	dec esi
	
	call print.string
	
;	mov al, '0'
;	call print.char
;	mov al, 'x'
;	call print.char
	
	mov ebx, 28
	mov ecx, 8
	.printLoop
		push edx
		push eax
		mov eax, edx
		
		push ecx
		mov ecx, ebx
		shr eax, cl
		mov ebx, ecx
		pop ecx
		
		and al, 0x0f
		pop edx
		mov ah, dh
		call print.nibble
		
		sub ebx, 4
		pop edx
	loop print.numberString.printLoop
popad
ret

; ah is color, al is byte
print.byte:
pushad
	mov bl, al
	shr al, 4
	and al, 0x0f
	call print.nibble
	mov al, bl
	and al, 0x0f
	call print.nibble
popad
ret

; ah is color, al is byte
print.binbyte:
pushad
	mov dl, al
	mov bl, 10000000b
	mov ecx, 8
	.printLoop:
		mov al, dl
		and al, bl
		jz print.binbyte.zero
		mov al, 0x31			; 1 in ascii
		call print.char
		shr bl, 1
		loop print.binbyte.printLoop
		jmp print.binbyte.done
		.zero:
			mov al, 0x30		; 0 in ascii
			call print.char
			shr bl, 1
			loop print.binbyte.printLoop
	.done:
popad
ret

ret
; ah is color, al is byte
print.nibble:
pushad
	cmp al, 0xa
	jge print.nibble.hexChar
		add al, 0x30
	jmp print.nibble.doPrint
	.hexChar
		add al, 0x57
	.doPrint:
		call print.char
popad
ret
	
print.string:
pushad
	; set color
	lodsb
	mov ah, al
	
	.printLoop
		lodsb
		cmp al, 0
		jz print.string.done
		call print.char
	jmp print.string.printLoop

	.done:
popad
ret

print.newLine:
pusha
	mov al, 0x0D
	call print.char
	mov al, 0x0A
	call print.char
popa
ret

print.scroll
pushad
push esi
;push edi
	mov ecx, 960
	mov esi, 0xb8000
	add esi, 160
	mov edi, 0xb8000
	rep movsd
	mov ax, 0x0
	mov ecx, 40
	rep stosd
	mov byte [draw.yPos], 24
;pop edi
pop esi
popad
ret

print.char:
pushad
	cmp al, 10
	jnz print.char.notLF
	; deal with lf
		inc byte [draw.yPos]
		cmp byte [draw.yPos], 25
		jne print.char.done
		call print.scroll
	jmp print.char.done
	
	.notLF:
	cmp al, 13
	jnz print.char.notCR
	; deal with nl
		mov byte [draw.xPos], 0
	jmp print.char.done
	
	.notCR:
		mov ecx, eax
		
		mov ebx, 0
		mov bl, [draw.xPos]
		shl bl, 1
		
		mov eax, 0
		mov al, [draw.yPos]
		mov edx, 160
		mul edx
		
		mov edi, 0xb8000
		add edi, eax
		add edi, ebx
		
		mov ax, cx
		cld
		stosw
		
		inc byte [draw.xPos]
	
	.done:
	call print.setCursor
popad
ret

print.setCursor:
pushad
	mov ebx, 0
	mov bl, [draw.xPos]
	
	mov eax, 0
	mov al, [draw.yPos]
	mov edx, 80
	mul edx
	
	add ebx, eax
	
	; low byte
	mov al, 0x0f
	mov dx, 0x03d4
	out dx, al
	
	mov al, bl
	mov dx, 0x03d5
	out dx, al
	
	; high byte
	mov al, 0x0e
	mov dx, 0x03d4
	out dx, al
	
	mov al, bh
	mov dx, 0x03d5
	out dx, al
popad
ret

print.versionInfo:
push esi
pushad
	mov esi, loadingMessages.versionInfo
	call print.string
	mov ax, 0x0476
	call print.char
	mov ah, 0x04
	mov al, [versionInfo.versionRelease]
	call print.nibble
	mov ax, 0x042e
	call print.char
	mov ah, 0x04
	mov al, [versionInfo.versionMinor]
	call print.nibble
	mov ax, 0x042e
	call print.char
	mov ah, 0x04
	mov al, [versionInfo.versionRevision]
	call print.byte
	mov esi, loadingMessages.buildInfo
	mov eax, [versionInfo.buildNumber]
	call print.numberString
	mov esi, loadingMessages.dateSeparator
	call print.string
	mov esi, versionInfo.buildDate
	call print.string
	call print.newLine
popad
pop esi
ret

loadingMessages:
	.versionInfo db 2,"interion ",0
	.buildInfo db 8,"  build ",0
	.dateSeparator db 8," - ",0