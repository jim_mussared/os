; protected mode first-stage kernel
; creation date: april 2003
;
; install as sectors 2-63 of first track

%include "display/color.asm"

[BITS 32]
[ORG 0x8000]
	
	; stage 5 - load the LDT
	
	; stage 6 - execute LTR instruction to load task register with TSS
	
	; stage 7 - reinit segment registers
	mov ax, 0x10
	mov ds, ax
	mov es, ax
	mov ss, ax
	mov sp, 0x4000
	mov ax, 0x00
	mov fs, ax
	mov gs, ax
	
	; stage 8 - execute LIDT to load the protected-mode interrupt vector table
	;			also programs the PICs
	call loadInterrupts
	
	; stage 9a - enable NMI
	in al, 0x70
	and al, 01111111b
	out 0x70, al
	
	call timers.rtc.init
	call timers.pit8253.init

	call ps2.init
	
	; stage 9b - enable interrupts
	sti
	
	call graphics.drawBootScreen
	
	push dword graphics.color.background
	push dword graphics.color.charcoal
	push dword 2
	push dword 2
	push dword loadingMessage.welcomeMessage
	call graphics.drawString
	add esp, 20
	
	push dword graphics.color.background
	push dword graphics.color.charcoal
	push dword 10
	push dword 74
	push dword versionInfo.buildDate
	call graphics.drawString
	add esp, 20
	
;	call print.cpuInfo
	
idleThread:
;hlt
jmp idleThread

loadingMessage:
	.welcomeMessage db "Jim's OS v0.0.1",0x0a,"Build date: ",0

%include "buildinfo.asm"

%include "display/graphicsmode.asm"
%include "display/bootscreen.asm"
%include "display/bootfont.asm"

%include "display/textmode.asm"

%include "interrupt/interrupt.asm"

%include "sys/cpuid.asm"

%include "sys/timers/rtc.asm"
%include "sys/timers/pit8253.asm"

%include "sys/input/ps2.asm"
%include "sys/input/kb.asm"

times 31744-($-$$) db 0
