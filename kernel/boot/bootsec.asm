; boot sector
; creation date: april 2003
;
; functionality:
; * partition information
; * switch to 0x13 vga mode
; * load first-stage image from sectors 2-63 (first track of partition)
; * load simple GDT and switch to protected mode
;
; install as first sector of partition

; 16 bit instructions, and dont do any offset mangling
[BITS 16]
[ORG 0]

; jump the nop
jmp short startBootSector
nop

; information about this volume for the OS to use later
; this is written when the volume is formatted
volumeInfo:
	.startSector db 0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00
	.numSectors db 0x00, 0x00, 0x00, 0x00
	.fsType db 0
	.bootDrive db 0

startBootSector:
	cli
		; the bios or the boot menu will have put us here
		mov bx, 0x7c0
		mov ds, bx
		
		; make a stack
		mov bx, 0x0
		mov ss, bx
		mov sp, 0x4000
	sti
	
	; set up cs
	jmp startBootSector.init
	.init:
	
	mov [volumeInfo.bootDrive], dl
	
	call bios.graphicsMode
	;call bios.textMode

	call bios.driveReset
	call bios.readSectors
	
	; second dot - we're ready to jump to protected mode
	call bios.drawDot

	mov ax, 0
	mov es, ax
	mov di, 0x500                   ; destination

	mov si, gdt                     ; source

	mov cx, 24                      ; length
	cld                             ; forward direction
	rep movsb                       ; move gdt to its new location
	
	
	; switching to protected mode - see section 9.9.1 in IA32 Volume 3
	
	; stage 1a - disable interupts
	cli
	
	; stage 1b - disable NMI
	; Port 0x70 is the real-time clock chip's index register; the MSB is the NMI enable bit.
	; set it to 1 to disable, 0 to enable
	in al, 0x70
	or al, 10000000b
	out 0x70, al
	in al, 0x71 ; otherwise the RTC is left in an undefined state
	
	; enable a20 gate
	call a20.enable

	; stage 2 - load gdt register
	lgdt [gdtptr]
		
	; stage 3 - set protected mode enable bit (note: need to set the PG flag too)
	mov eax,cr0
	or al,1
	mov cr0,eax
	
	; stage 4 - jmp to serialize CPU - set cs/ip/eip
	; go to the next sector
	jmp	dword 0x8:0x8000		; flush prefetch queue

bios.drawDot:
	;pusha
	;	mov ah, 0x0e		; write character
	;	mov al, 0x2e		; ascii for '.'
	;	int 0x10			; call bios
	;popa
ret

bios.printSZ:
; input : ds:si points to zero terminated string
	;pusha
	;	cld					; direction forward
	;	lodsb				; get next character
	;	cmp al, 0x0
	;	jz bios.printSZdone
	;	mov ah, 0x0e		; write character
	;	int 0x10			; call bios
	;	jmp bios.printSZ
	;bios.printSZdone:
	;popa
ret

bios.graphicsMode:
	push ax
		mov al, 0x13
		mov ah, 0x0
		int 0x10
	pop ax
ret

bios.textMode:
	push ax
		mov al, 0x03
		mov ah, 0x00
		int 0x10
		
		mov si, messages.bootMsg
		call bios.printSZ
	pop ax
ret

bios.readSectors:
	pusha
		; first dot - reading sectors
		call bios.drawDot
		
		call bios.testReadExt
		
		jnc bios.readSectors.doRead
		
		; no extensions available
		call bios.textMode
		mov si, errors.noExtensions
		call bios.printSZ
		jmp $
			
		; use extensions
		.doRead:
			; second dot - reading sectors
			call bios.drawDot
			
			; now load the pmode image (blocks 1-63 of this track)
			
			mov [i13ex_rqp.numBlocks], word 62
			mov [i13ex_rqp.dest], dword 0x00008000
			
			push word 0x7c0
			pop es
			mov si, volumeInfo.startSector
			mov di, i13ex_rqp.lbaAddr
			
			lodsb
			inc al
			stosb
			
			cld
			mov cx, 7
				rep movsb
			
			mov dl, [volumeInfo.bootDrive]
			mov si, i13ex_rqp
			
			mov cx, 5 ; five tries at reading
			.tryReadAgain:
				mov ah, 0x42
				int 13h
				jnc bios.readSectors.diskReadSuccessful
			loop bios.readSectors.tryReadAgain
			
			; too many errors
			call bios.textMode
			mov si, errors.driveRead
			call bios.printSZ
			jmp $     ; die
			
			.diskReadSuccessful:
			
			; third dot - loaded the second stage image
			call bios.drawDot
	popa
ret

bios.driveReset:
	push ax
		xor ah, ah
		int 0x13
		jnc bios.driveReset.success
			; msg that we have a serious drive error
			call bios.textMode
			mov si, errors.driveReset
			call bios.printSZ
			jmp $
		.success:
	pop ax
ret

bios.testReadExt:
	pusha
		mov ah, 0x41					; service number
		mov bx, 0x55aa					; will be reversed if success
		mov dl, [volumeInfo.bootDrive]
		int 0x13
		jc bios.testReadExt.failExt
		cmp bx, 0xaa55
		jnz bios.testReadExt.failExt
		jz bios.testReadExt.successExt
		.failExt:
			stc
			jmp bios.testReadExt.finish
		.successExt:
			clc
		.finish:
	popa
ret

a20.enable:
	pusha
		; enable a20 line
		a20.wait1:
			in al, 0x64
			test al, 2
			jnz a20.wait1
				mov al, 0xd1
				out 0x64, al
		a20.wait2:
			in al, 0x64
			test al, 2
			jnz a20.wait2
				mov al, 0xdf
				out 0x60, al
	popa
ret

; the packet to send to the bios to do the first reads
; i13ex_rqp = interrupt 0x13 extensions request packet
i13ex_rqp:
	.packetSize		db 0x10		; size of request packet
	.reserved		db 0x0		; reserved
	.numBlocks		dw 0x0		; number of blocks
	.dest			dd 0x0		; where to load them
	.lbaAddr		dd 0x0
					dd 0x0		; lba address of starting block
	
errors:
	.badCpu			db 13,10,'CPU not supported',0
	.driveReset		db 13,10,'Drive Reset Fail',0
	.driveRead		db 13,10,'Drive Read Fail',0
	.noExtensions	db 13,10,'i13 ex bad',0

messages:
	.bootMsg		db 'hello boot world',0


gdtptr dw 0x7ff ; limit (256 slots)
dd 0x500 ; base (physical address)

; null selector (required)
gdt dw 0, 0, 0, 0

; kernel code descriptor
dw 0xffff     ; segment limit (4 gb total)
dw 0          ; base address (bits 0-15)
db 0          ; base address (bits (16-24)
db 10011000b  ; dpl 0, code (execute only)
db 11001111b  ; granlurarity (4k), 32-bit, limit high nibble = f
db 0          ; base address (bits 24-32)

; kernel data descriptor
dw 0xffff     ; segment limit (4 gb total)
dw 0          ; base address (bits 0-15)
db 0          ; base address (bits (16-24)
db 10010010b  ; dpl 0, data (read/write)
db 11001111b  ; granlurarity (4k), 32-bit, limit high nibble = f
db 0          ; base address (bits 24-32)


times 510-($-$$) db 0		; pad it out to 510 bytes
dw 0xAA55					; Tell the bios/mbr-bootsector we're bootable


;	
;	; pic
;	mov al, 0xff
;	out 0xa1, al
;	
;	; disable all irqs except irq2
;	mov al, 0xfb
;	out 0x21, al
;
