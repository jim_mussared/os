print.cpuInfo:
pushad
	; disable the cpu serial number (for pentium 4)
	mov ecx, 119
	rdmsr			; into edx:eax
	push eax		; we'll re-enable it later if it was set
	and eax, 0xffdfffff
	wrmsr

	xor eax, eax
	cpuid
	mov ecx, eax
	inc ecx
	xor eax, eax
	
	.cpuidLoop
		push ecx
		push eax
		
			mov esi, print.cpuInfo.cpuidMessage
			call print.numberString
			mov esi, print.cpuInfo.cpuidMessage2
			call print.string
			
			cpuid
			
			push edx
			push ecx
			push ebx
			mov esi, print.cpuInfo.registerDisplayEAX
			call print.numberString
			pop eax
			mov esi, print.cpuInfo.registerDisplayEBX
			call print.numberString
			pop eax
			mov esi, print.cpuInfo.registerDisplayECX
			call print.numberString
			pop eax
			mov esi, print.cpuInfo.registerDisplayEDX
			call print.numberString
			mov esi, print.cpuInfo.cpuidMessage3
			call print.string
			call print.newLine
		
		pop eax
		pop ecx
		inc eax
		loop print.cpuInfo.cpuidLoop
		
	mov ecx, 119
	rdmsr
	pop eax		; get back the old value of eax
	wrmsr
popad
ret
.cpuidMessage db 7,"cpuid(",0
.cpuidMessage2 db 7,") ",0
.registerDisplayEAX db 8,"a(",0
.registerDisplayEBX db 8,") b(",0
.registerDisplayECX db 8,") c(",0
.registerDisplayEDX db 8,") d(",0
.cpuidMessage3 db 8,")",0