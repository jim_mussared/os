ps2.init:
pushad
	; reset the ps2 device
	mov al, 0xd4
	out 0x64, al
	mov al, 0xff	; reset device
	out 0x60, al
	; put it into data reporting mode
	mov al, 0xd4
	out 0x64, al
	mov al, 0xf4	; set data reporting mode on
	out 0x60, al
popad
ret
