timers.rtc.init:
pushad

	; Write register index to port 70h
	; Read/write port 71h to get/set data


	; program the real time clock to - 8Hz
	mov al, 0x0a		; status register a
	out 0x70, al
	;in al, 0x71
	mov al, 00101001b	; [update in progress] [][][22 stage divider 010=32.768Hz] [][][][Frequency divider (/256 --> 128Hz)]
	out 0x71, al
	
	mov al, 0x0b		; status register b
	out 0x70, al
	;in al, 0x71
	xor al, al
	or al, 01000010b	; [disable clock update][enable periodic update (freq)][enable alarm interrupt][enable updated ended interrupt (time change, once per second)][enable square wave frequency (not available on at)][time date in binary (0=bcd)][24 hour mode][enable daylight savings]
	out 0x71, al

popad
ret