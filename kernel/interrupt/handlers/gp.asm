interruptHandler.gp:
	
	mov esi, gpInterruptMessage
	call print.string
	call print.newLine
	
	mov ecx, 10
	.stackCrawl
		pop eax
		mov esi, gpInterruptCodeMessage
		call print.numberString
		call print.newLine
		loop interruptHandler.gp.stackCrawl
	
	jmp $
iret

gpInterruptMessage db 4,"General Protection Fault. Stack crawl follows:",0
gpInterruptCodeMessage db 2,"  ",0