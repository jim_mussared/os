interruptHandler.undefined:
pushad
push ds
push es
push fs
push gs

	mov esi, undefinedInterruptMessage
	call print.string
	call print.newLine
	
pop gs
pop fs
pop es
pop ds
popad
iret

undefinedInterruptMessage db 1,"Unknown interrupt",0