;	template for interrupt gate in the IDT - see Fig5-2 in ia32 vol 3
;	dw 0x0000		; offset 0-15
;	dw 0x0008		; segment selector
;	db 0x00			; zero
;	db 10001110b	; (Present)(DPL)(DPL)(0)(D 32bits)(1)(1)(0)
;	dw 0x0000		; offset 16-31

loadInterrupts:
	lidt [idtptr]
	
	; program the PIC's to set up IRQs

	; send ICW-1
	mov al, 00010001b ; [0][0][0][1][0=Edge/1=Level][0][0=MasterSlave/1=MasterOnly][0=No ICW4/1=ICW4]
	out 0x20, al		; pic 1
	call delay
	out 0xa0, al		; pic 2
	call delay
	
	; now say where to map the irqs - ICW-2
	mov al, 0x20
	out 0x21, al		; irq 0-7  --> int 32-39
	call delay
	mov al, 0x28
	out 0xa1, al		; irq 8-15 --> int 40-47
	call delay
	
	; tell the pic's how to communicate with eachother (irq 2) - ICW-3  (0=perhifial device, 1=slave)
	mov al, 00000100b
	out 0x21, al
	call delay
	mov al, 0x02 ; which irq on the master we're connected to
	out 0xa1, al
	call delay
	
	; ICW-4 - 80x86 hardware & non-automatic EOI [0][0][0][SpecialFullyNestedMode][BufferedMode][Slave/Master][Manual/Auto EOI][MCS-8085-Mode/8086-88-Mode]
	mov al, 0x01
	out 0x21, al
	call delay
	mov al, 0x01
	out 0xa1, al
	call delay
	
	; enable all interrupts
	
	;mov al, 11111111b
	
	;		??? HDD NCP PS2 ??? ??? LNK RTC
	mov al, 11101100b
	out 0xa1,al		; slave
	call delay
	;		LPT1 FLP LPT2 COM1 COM2 LNK KBD STM
	mov al, 11111000b
	;mov al, 0x00
	out 0x21,al		; master
	call delay
ret

delay:
	push eax
		in al, 0x80
		in al, 0x80
	pop eax
ret

displayInteruptMessageEx:
push ebp
mov ebp, esp
pushad
	push dword graphics.color.background
	push dword [ebp+12]
	push dword 182
	push dword 2
	push dword [ebp+8]
	call graphics.drawString
	add esp, 20
popad
mov esp, ebp
pop ebp
ret

displayInteruptMessage:
push ebp
mov ebp, esp
pushad
	push dword graphics.color.background
	push dword [ebp+12]
	push dword 190
	push dword 2
	push dword [ebp+8]
	call graphics.drawString
	add esp, 20
popad
mov esp, ebp
pop ebp
ret

idtptr
dw 0x0800					; limit (256*8)
dd interruptDescriptorTable	; base (physical address)

interruptDescriptorTable

; int 0x00 - #DE - divide error
	dw interruptHandler.divide
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x01 - #DB - debug
	dw interruptHandler.debug
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x02 -     - nmi
	dw interruptHandler.nmi
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x03 - #BP - breakpoint
	dw interruptHandler.breakpoint
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x04 - #OF - overflow
	dw interruptHandler.overflow
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x05 - #BR - BOUND range exceeded
	dw interruptHandler.bound
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x06 - #UD - invalid opcode
	dw interruptHandler.invop
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x07 - #NM - device not available
	dw interruptHandler.nmc
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x08 - #DF - double fault
	dw interruptHandler.double
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x09 -     - coprocessor segment overrun
	dw interruptHandler.cpseg
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x0a - #TS - invalid TSS
	dw interruptHandler.invtss
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x0b - #NP - segment not present
	dw interruptHandler.segnp
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x0c - #SS - stack segment fault
	dw interruptHandler.ssfault
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x0d - #GP - general protection
	dw interruptHandler.gp
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x0e - #PF - page fault
	dw interruptHandler.pf
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x0f -     - (reserved)
	dw 0x0000
	dw 0x0008
	db 0x00
	db 00001110b	; not present
	dw 0x0000

; int 0x10 - #MF - x87 FPU floating point error
	dw interruptHandler.fperr
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x11 - #AC - alignment check
	dw interruptHandler.alignchk
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x12 - #MC - machine check
	dw interruptHandler.machchk
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; int 0x13 - #XF - simd floating point exception
	dw interruptHandler.simd
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

;int 0x14-->0x1f - reserved
%rep 12
	dw 0x0000
	dw 0x0008
	db 0x00
	db 00001110b	; not present
	dw 0x0000
%endrep

; int 0x20 - mapped irq 0x00
	dw interruptHandler.irq0
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x21 - mapped irq 0x01
	dw interruptHandler.irq1
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x22 - mapped irq 0x02
	dw interruptHandler.irq2
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x23 - mapped irq 0x03
	dw interruptHandler.irq3
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x24 - mapped irq 0x04
	dw interruptHandler.irq4
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x25 - mapped irq 0x05
	dw interruptHandler.irq5
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x26 - mapped irq 0x06
	dw interruptHandler.irq6
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x27 - mapped irq 0x07
	dw interruptHandler.irq7
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x28 - mapped irq 0x08
	dw interruptHandler.irq8
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x29 - mapped irq 0x09
	dw interruptHandler.irq9
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x2a - mapped irq 0x0a
	dw interruptHandler.irqa
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x2b - mapped irq 0x0b
	dw interruptHandler.irqb
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x2c - mapped irq 0x0c
	dw interruptHandler.irqc
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x2d - mapped irq 0x0d
	dw interruptHandler.irqd
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x2e - mapped irq 0x0e
	dw interruptHandler.irqe
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
	
; int 0x2f - mapped irq 0x0f
	dw interruptHandler.irqf
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000

; the undefined interrupts
%rep 208
	dw interruptHandler.undefined
	dw 0x0008
	db 0x00
	db 10001110b
	dw 0x0000
%endrep

%include "interrupt/handlers/divide.asm"
%include "interrupt/handlers/debug.asm"
%include "interrupt/handlers/nmi.asm"
%include "interrupt/handlers/breakpoint.asm"
%include "interrupt/handlers/overflow.asm"
%include "interrupt/handlers/bound.asm"
%include "interrupt/handlers/invop.asm"
%include "interrupt/handlers/nmc.asm"
%include "interrupt/handlers/double.asm"
%include "interrupt/handlers/cpseg.asm"
%include "interrupt/handlers/invtss.asm"
%include "interrupt/handlers/segnp.asm"
%include "interrupt/handlers/ssfault.asm"
%include "interrupt/handlers/gp.asm"
%include "interrupt/handlers/pf.asm"
%include "interrupt/handlers/fperr.asm"
%include "interrupt/handlers/alignchk.asm"
%include "interrupt/handlers/machchk.asm"
%include "interrupt/handlers/simd.asm"

%include "interrupt/irq/irq0.asm"
%include "interrupt/irq/irq1.asm"
%include "interrupt/irq/irq2.asm"
%include "interrupt/irq/irq3.asm"
%include "interrupt/irq/irq4.asm"
%include "interrupt/irq/irq5.asm"
%include "interrupt/irq/irq6.asm"
%include "interrupt/irq/irq7.asm"
%include "interrupt/irq/irq8.asm"
%include "interrupt/irq/irq9.asm"
%include "interrupt/irq/irqa.asm"
%include "interrupt/irq/irqb.asm"
%include "interrupt/irq/irqc.asm"
%include "interrupt/irq/irqd.asm"
%include "interrupt/irq/irqe.asm"
%include "interrupt/irq/irqf.asm"

%include "interrupt/handlers/undefined.asm"
