interruptHandler.irq6:
push esi
	mov esi, interruptHandler.irq6.message
	call print.string
	call print.newLine
pop esi

mov al, 0x20
out 0x20, al
iret
.message db "IRQ 0x06 (Floppy)",0