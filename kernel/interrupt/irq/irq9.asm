interruptHandler.irq9:
push esi
	mov esi, interruptHandler.irq9.message
	call print.string
	call print.newLine
	
	xor eax, eax
	in al, 0x64
	in al, 0x60
pop esi

mov al, 0x20
out 0x20, al
out 0xA0, al
iret
.message db "IRQ 0x09",0