interruptHandler.irqc:
pushad

	push dword graphics.color.background
	push dword graphics.color.charcoal
	push dword 50
	push dword 2
	push dword interruptHandler.irqc.message
	call graphics.drawString
	add esp, 8
	
	xor eax, eax
	in al, 0x64
	in al, 0x60
	mov ebx, eax
	
	mov eax, 20
	mul byte [interruptHandler.irqc.byteIndex]
	add eax, 88
	push eax
	push ebx
		call graphics.drawByte
	add esp, 20
	
	inc byte [interruptHandler.irqc.byteIndex]
	cmp byte [interruptHandler.irqc.byteIndex], 3
	jne interruptHandler.irqc.continuePacket
	mov byte [interruptHandler.irqc.byteIndex], 0
	.continuePacket
	
	mov al, 0x20
	out 0x20, al
	out 0xA0, al

popad
iret

.byteIndex db 0
.message db "PS/2 Mouse: 0x",0