interruptHandler.irq0:
pushad
	
	push dword graphics.color.background
	push dword graphics.color.blue
	push dword 182
	push dword 2
	push dword [interruptHandler.irq0.timerCount]
	call graphics.drawDWord
	add esp, 20
	inc dword [interruptHandler.irq0.timerCount]

	mov al, 0x20
	out 0x20, al
	out 0xA0, al

popad
iret

.timerCount dd 0
