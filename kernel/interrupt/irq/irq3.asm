interruptHandler.irq3:
push esi
	mov esi, interruptHandler.irq3.message
	call print.string
	call print.newLine
pop esi

mov al, 0x20
out 0x20, al
iret
.message db "IRQ 0x03 (COM2)",0