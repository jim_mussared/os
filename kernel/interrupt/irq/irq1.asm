interruptHandler.irq1:
pushad

	push dword graphics.color.background
	push dword graphics.color.charcoal
	push dword 40
	push dword 2
	push dword interruptHandler.irq1.message
	call graphics.drawString
	add esp, 8
	
	xor eax, eax
	in al, 0x64
	in al, 0x60
	mov bl, al
	and al, 01111111b
	
	push dword 72
	push eax
		call graphics.drawByte
	add esp, 8
	
	push dword 88
	and bl, 10000000b
	jnz interruptHandler.irq1.breakCode
	push dword interruptHandler.irq1.makeCodeMessage
	jmp interruptHandler.irq1.done
	.breakCode:
	push dword interruptHandler.irq1.breakCodeMessage
	.done:
	
	
	call graphics.drawString
	add esp, 20

	mov al, 0x20
	out 0x20, al
	out 0xA0, al
	
popad
iret

.makeCodeMessage db "down",0
.breakCodeMessage db "up  ",0
.message db "Keyboard: 0x",0