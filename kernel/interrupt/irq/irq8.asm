interruptHandler.irq8:
pushf
pushad
		
	mov al, 0x0c		; status register c
	out 0x70, al
	in al, 0x71
	; Bit 4 (UF): 	1=time changed (generated every second)
	; Bit 5 (AF): 	1=alarm time reached
	; Bit 6 (PF): 	1=periodic int
	; Bit 7 (IRQF):	1=interupt request
	; 4,5,6 are set regardless of setting in register b, so check 7 as well
	
	; Test that it is an interupt caused by the periodic timer
	;		10010000
	cmp al, 11000000b
	jne interruptHandler.irq8.ignoreTimerInterupt
	
	push dword graphics.color.background
	push dword graphics.color.red
	push dword 182
	push dword 120
	push dword [interruptHandler.irq8.timerCount]
	call graphics.drawDWord
	add esp, 20
	inc dword [interruptHandler.irq8.timerCount]
	
	.ignoreTimerInterupt:

	mov al, 0x20
	out 0x20, al
	out 0xA0, al
	
popad
popf
iret

.timerCount dd 0
