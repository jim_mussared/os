interruptHandler.irq5:
push esi
	mov esi, interruptHandler.irq5.message
	call print.string
	call print.newLine
pop esi

mov al, 0x20
out 0x20, al
iret
.message db "IRQ 0x05 (LPT2)",0