interruptHandler.irq7:
push esi
	mov esi, interruptHandler.irq7.message
	call print.string
	call print.newLine
pop esi

mov al, 0x20
out 0x20, al
iret
.message db "IRQ 0x07 (LPT1)",0