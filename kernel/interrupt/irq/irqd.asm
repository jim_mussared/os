interruptHandler.irqd:
push esi
	mov esi, interruptHandler.irqd.message
	call print.string
	call print.newLine
pop esi

mov al, 0x20
out 0x20, al
out 0xA0, al
iret
.message db "IRQ 0x0d",0