[BITS 16]

jmp short entry_lbl
nop

entry_lbl:

	cli
		; make us an 8kb stack area (0x9000->0xafff)
		mov ax, 0x9000
		mov ss, ax
		mov sp, 0x1FFF
		
		; relocate
		xor di, di
		xor si, si
		push word 0x07c0
		pop ds
		
		mov bx, 0x0600
		mov es, bx
		
		cld
		mov cx, 512
			rep movsb
		
		; so the offsets are correct
		mov ds, bx
	sti
	
	jmp 0x600:begin
	begin:
	
	; clear the screen
	push word 0xb800
	pop word es
	xor di, di
	xor ax, ax
	mov cx, 2000
		rep stosw
	
	; welcome message
	push word title_str
	push word 164
	call display_string
	add sp, 4
	
	; print out the os names
	mov cx, [num_os]
	inc cx
	mov bx, os_1
	mov ax, 832
	list_loop:
		push word bx
		push word ax
		call display_string
		add sp, 4
		add bx, 17
		add ax, 80
		loop list_loop
	
	call highlight_option
	
	wait_for_key:
		xor ah, ah
		int 0x16
		
		call highlight_option
		
		cmp ah, 0x48
		jz got_up_arrow_key
		
		cmp ah, 0x50
		jz got_down_arrow_key
		
		cmp ah, 0x1c
		jz selection_made
		
		handled_key:
		
		call highlight_option
		
		jmp wait_for_key
	
	selection_made:
		; now read in the boot sector
		
		mov cx, 5 ; five tries
		
		try_read_again:

			push word 0x0600
			pop es
			
			mov si, 5				; set lba addr param packet
			imul si, [current_sel]
			add si, lba_start
			mov di, lba_addr
			movsd
			
			mov dl, [es:si]			; dl = drive number
						
			mov ah, 0x42			; use function 0x42 (extended read)
			mov si, int13h_data_packet
			
			int 13h
			
			jnc disk_read_successful
			loop try_read_again
		
		; too many errors
		push word error_msg
		push word 1632
		push word 0xb800
		pop es
		call display_string
		add sp, 4
		
		jmp $     ; die
	
disk_read_successful:

	jmp 0x7c0:0

got_down_arrow_key:
	mov ax, [num_os]
	cmp word [current_sel], ax	; make sure we're not at the bottom
	jz handled_key				; we are, so ignore this keypress
	inc word [current_sel]		; we're not, so move current_sel down one
	jmp handled_key				; we've handled the key
		
got_up_arrow_key:		
	cmp word [current_sel], 0	; make sure we're not at the top
	jz handled_key				; we are, so ignore this keypress
	dec word [current_sel]		; we're not, so move current_sel up one
	jmp handled_key				; we've handled the key

highlight_option:
	enter 0,0
	pusha
	push ds
		mov si, [current_sel]	; work out where the line starts (where the arrow is)
		imul si, 160
		add si, 1660
		
		mov di, [current_sel]	; we're writing to the same place
		imul di, 160
		add di, 1660
		
	;	mov ax, 0xb800			; set up es & ds so that es:di and ds:si are correct
	;	mov es, ax
	;	mov ds, ax
		
		push word 0xb800
		pop word ds
		
	;	xor ax, ax				; clear ax
		lodsw					; load the existing value (0x0000 for not selected, 0x8caf for flashing red arrow)
		xor ax, 0x8caf			; invert it
		stosw					; save it back.  es:di and ds:si are now pointing to the blank between the arrow
								; and the OS name
		
		add si, 2				; now make them point to the OS name
		add di, 2
		
		mov cx, 16				; names are 16 chars
		change_color_hl_loop:
			lodsw				; read the word (and add 2 to si)
			xor ah, 8			; invert the bright bit
			stosw				; save it back (and add 2 to di)
			loop change_color_hl_loop
	
	pop ds
	popa
	leave
	ret

display_string:
	enter 0,0
	pusha
	;	push word 0xb800
	;	pop word es			; es:di starts at beginning of color vid mem
		mov di, [bp + 4]		; where to start writing
		imul di, 2			; but color, so 2bytes per char
		mov si, [bp + 6]		; the beginning of the string to write (the color value actually)
		mov cx, 16			; all strings are 16 chars long
		lodsb				; now si points to the real beginning of the string
		cld				;  and al is the color
		copy_loop:
			movsb			; [es:di] = [ds:si]  (inc di and si also)
			stosb			; [es:di] = al (the color)   (inc di)
			loop copy_loop
	popa
	leave
	ret
	
; the packet to send to the bios to read the bootsector.
int13h_data_packet:
db 0x10		; size
db 0x0		; reserved
dw 0x1		; number of blocks
dd 0x07c00000	; where to load them
lba_addr db 0x0, 0x0, 0x0, 0x0
db 0x0, 0x0, 0x0, 0x0	; where on the disk (lba)



; all the data goes here
;	2 bytes		-	Default selection
;	16 bytes	-	Title message
;	4 x 17 bytes-	Color byte + OS name

current_sel dw 2

title_str db 2,"Jim's Boot Menu "
error_msg db 0x8c,"COULDN'T LOAD OS"

num_os dw 3

os_1 db 1,"BeOS"
times 12 db ' '

os_2 db 5,"Linux"
times 11 db ' '

os_3 db 4,"Windows XP"
times 6 db ' '

os_4 db 6,"Jim Dev"
times 9 db ' '

lba_start	db 0x40, 0x46, 0xc3, 0x01, 0x80
			db 0x3f, 0x00, 0x00, 0x00, 0x80
			db 0xd5, 0x29, 0x00, 0x01, 0x80
			db 0x0, 0x0, 0x0, 0x0, 0x80

times 510-($-$$) db 0		; pad it out to 510 bytes
dw 0xAA55					; Tell the bios we're bootable
